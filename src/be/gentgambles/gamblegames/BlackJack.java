package be.gentgambles.gamblegames;

import java.util.Random;
import java.util.Scanner;

public class BlackJack {
    public static int blackJack(int gameCoins) {
        int totalCoins = gameCoins;
        boolean wantToPlay = true;
        Scanner scanner = new Scanner(System.in);

        while (wantToPlay) {
            /**
             * aanmaken score
             */
            int scoreSpeler = 0;
            int ronde = 0;
            char keuze = 'Y';

            /**
             * aanmaken deck kaarten
             */
            int deckKaarten[] = new int[52];
            for (int suits = 0; suits < 4; suits++) {
                for (int value = 1; value <= 13; value++) {
                    deckKaarten[value - 1 + (suits * 13)] = value;
                }
            }
            /**
             * randomizen van het deck
             */
            Random rand = new Random();
            for (int i = 0; i < deckKaarten.length; i++) {
                int randomIndexToSwap = rand.nextInt(deckKaarten.length);
                int temp = deckKaarten[randomIndexToSwap];
                deckKaarten[randomIndexToSwap] = deckKaarten[i];
                deckKaarten[i] = temp;
            }

            totalCoins = totalCoins - 5;
            /**
             * spel starten
             */
            System.out.println("Probeer tot aan 21 te geraken. \nWanneer u er over gaat wint u niets. \nWanneer u te vroeg stopt wint u niets. \nIndien u op tijd stopt krijgt u 15 gameCoins. \nIndien u precies op 21 land krijgt u 100 gameCoins!\n \n");
            System.out.println("Uw eerste kaart heeft waarde :" + deckKaarten[ronde]);
            scoreSpeler = deckKaarten[ronde];
            while (keuze == 'Y' || keuze == 'y') {
                while (keuze != 'N' && keuze != 'n') {
                    System.out.println("Wilt u nog een kaart? [Y/N]");
                    keuze = scanner.next().charAt(0);
                    if (keuze == 'y' || keuze == 'Y') {
                        System.out.println("u heeft ja gekozen");
                        keuze = 'Y';
                        ++ronde;
                        scoreSpeler = scoreSpeler + ((int) deckKaarten[ronde]);
                        System.out.println(scoreSpeler);
                    }else if (keuze == 'n' || keuze == 'N'){
                        ++ronde;
                        int scoreVolgendeKaart = scoreSpeler + ((int) deckKaarten[ronde]);
                        System.out.println("De volgende kaart had als waarde: " +deckKaarten[ronde]+". \nUw totale waarde zou " + scoreVolgendeKaart + " zijn.");
                        if (scoreVolgendeKaart <= 21) {
                            System.out.println("Je hebt te vroeg afgehaakt, je verdient niets.");
                        }else if (scoreVolgendeKaart > 21){
                            System.out.println("Je hebt op tijd afgehaakt! Je verdient 15 gameCoins");
                            totalCoins = totalCoins + 15;
                        }
                    }
                    if (scoreSpeler == 21){
                        System.out.println("JE HEBT EXACT 21 GEHAALD! JACKPOT!");
                        totalCoins = totalCoins + 100;
                        keuze = 'n';
                    }else if (scoreSpeler > 21){
                        System.out.println("Je hebt meer dan 21 gehaald");
                        keuze = 'n';
                    }
                }
            }


            /**
             * Einde spel, indien mogelijk de keuze om opnieuw te spelen.
             */
            if (totalCoins < 5) {
                wantToPlay = false;
                break;
            }
            char c = 'a';
            while (c != 'y' && c != 'Y' && c != 'N' && c != 'n') {

                System.out.println("U heeft nog " + totalCoins + " gameCoins over.");
                System.out.println("Wilt u nog eens spelen? [Y/N]");
                c = scanner.next().charAt(0);
            }
            if (c == 'n' || c == 'N') {
                wantToPlay = false;
            }
        }
        return totalCoins;
    }
}