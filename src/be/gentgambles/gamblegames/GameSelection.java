package be.gentgambles.gamblegames;

import java.util.Scanner;

public class GameSelection {
    public static int selectGame(int gamecoins) {
        Scanner scanner = new Scanner(System.in);

        /**
         * Keuzeselectie
         */
        System.out.println("u heeft " + gamecoins + " coins om mee te spelen");
        System.out.println("Welk spel zou u graag spelen?");
        System.out.println("Voor hoger lager: druk 1");
        System.out.println("Voor Lottotrekking: druk 2");
        System.out.println("Voor Slot machine: druk 3");
        System.out.println("Voor Blackjack: druk 4");
        System.out.println("Voor afsluit: druk 5");
        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                int val = scanner.nextInt();
                if (val >= 1 && val <= 5) {
                    return val;
                }
                System.out.println("Gelieve een keuze te maken");
            } else {
                System.out.println("Gelieve een keuze te maken");
                scanner.next();
            }
        }
        int selection = scanner.nextInt();
        return selection;
    }
}
