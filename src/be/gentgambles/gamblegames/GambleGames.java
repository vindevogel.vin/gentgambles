package be.gentgambles.gamblegames;

import static be.gentgambles.gamblegames.BlackJack.blackJack;
import static be.gentgambles.gamblegames.GameSelection.selectGame;
import static be.gentgambles.gamblegames.HogerLager.hogerLager;
import static be.gentgambles.gamblegames.Lotto.lotto;
import static be.gentgambles.gamblegames.SlotMachine.slotMachine;

public class GambleGames {
    public static void main(String[] args) {

        /**
         * welkomstbericht
         */
        System.out.println("Gent gambles presenteert met trots!");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.println("The digital gambling slot");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }

        /**
         * declaratie van de globale variabele
         */
        int gameCoins = 25;

        /**
         * core game loop
         */
        do {
            int nextGame = selectGame(gameCoins);
            switch (nextGame) {
                case 1:
                    gameCoins = hogerLager(gameCoins);
                    break;
                case 2:
                    gameCoins = lotto(gameCoins);
                    break;
                case 3:
                    gameCoins = slotMachine(gameCoins);
                    break;
                case 4:
                    gameCoins = blackJack(gameCoins);
                    break;
                case 5:
                    System.out.println("Afsluiten");
                    System.exit(0);
                    break;
            }
        } while (gameCoins >= 5);


        /**
         * einde van spel
         */
        System.out.println("U heeft niet voldoende coins, u kan niet meer spelen.\nGelieve er meer aan te schaffen (door het programma opnieuw op te starten ;)");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.exit(0);

    }
}
