package be.gentgambles.gamblegames;

import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Lotto {
    public static int lotto(int gameCoins) {
        int totalCoins = gameCoins;
        boolean wantToPlay = true;
        Scanner scanner = new Scanner(System.in);

        while (wantToPlay) {
            totalCoins = totalCoins - 5;
            int gelijkeNummers = 0;
            /**
             * lottoNummers genereren
             */
            int lottoNummers[] = new int[6];
            for(int i = 0; i <  lottoNummers.length; i++) {
                lottoNummers[i] = (int) (Math.random()*45);
            }

            /**
             * lottoNummers laten invullen
             */
            int gekozenNummers[] = new int[6];
            int locatieGekozenNummer = 0;
            for (locatieGekozenNummer = 0; locatieGekozenNummer < gekozenNummers.length; locatieGekozenNummer++) {
                System.out.println("Gelieve een cijfer tussen 1 en 45 kiezen");
                while (scanner.hasNext()) {
                    if (scanner.hasNextInt()) {
                        int val = scanner.nextInt();
                        if (val >= 1 && val <= 45) {
                            gekozenNummers[locatieGekozenNummer] = val;
                            break;
                        }
                        System.out.println("Gelieve een cijfer tussen 1 en 45 kiezen");
                    } else {
                        System.out.println("Gelieve een cijfer tussen 1 en 45 kiezen");
                        scanner.next();
                    }
                }
            }
            for(int gekozenWaarde = 0; gekozenWaarde < gekozenNummers.length; gekozenWaarde++) {
                for(int randomWaarde = 0; randomWaarde < lottoNummers.length; randomWaarde++){
                    if (gekozenNummers[gekozenWaarde] == lottoNummers[randomWaarde]) {
                        ++gelijkeNummers;
                    }
                }
            }
            System.out.println("Uw cijfers: ");
            for(int i = 0; i <  gekozenNummers.length; i++) {
                System.out.print(gekozenNummers[i] + " ");
            }
            System.out.println("\n" + "De lotto cijfers: ");
            for(int i = 0; i < lottoNummers.length; i++) {
                System.out.print(lottoNummers[i] + " ");
            }

            System.out.println("\n"+"U heeft " + gelijkeNummers + " cijfer(s) correct");
            switch (gelijkeNummers){
                case 1:
                    System.out.println("Je hebt 3 coins gewonnen");
                    totalCoins = totalCoins + 3;
                    break;
                case 2:
                    System.out.println("Je hebt 5 coins gewonnen");
                    totalCoins = totalCoins + 5;
                    break;
                case 3:
                    System.out.println("Je hebt 15 coins gewonnen");
                    totalCoins = totalCoins + 15;
                    break;
                case 4:
                    System.out.println("Je hebt 45 coins gewonnen");
                    totalCoins = totalCoins + 45;
                    break;
                case 5:
                    System.out.println("Je hebt 75 coins gewonnen");
                    totalCoins = totalCoins + 75;
                    break;
                case 6:
                    System.out.println("Je hebt 140 coins gewonnen");
                    totalCoins = totalCoins + 140;
                    break;
                default:
                    System.out.println("Je hebt geen coins gewonnen, je hebt nog " + totalCoins + " over");
                    break;
            }





            /**
             * Einde spel, indien mogelijk de keuze om opnieuw te spelen.
             */
            if (totalCoins <5){
                wantToPlay = false;
                break;
            }
            char c = 'a';
            while (c != 'y' && c != 'Y' && c != 'N' && c != 'n') {
                System.out.println("Wilt u nog eens spelen? [Y/N]");
                c = scanner.next().charAt(0);
            }
            if (c == 'n' || c == 'N') {
                wantToPlay = false;
            }
        }
        return totalCoins;
    }
}
