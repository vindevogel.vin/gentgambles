package be.gentgambles.gamblegames;

import com.sun.source.tree.WhileLoopTree;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import static be.gentgambles.gamblegames.BlackJack.blackJack;
import static be.gentgambles.gamblegames.Lotto.lotto;
import static be.gentgambles.gamblegames.SlotMachine.slotMachine;

public class HogerLager {
    public static int hogerLager(int gameCoins) {
        int totalCoins = gameCoins;
        boolean wantToPlay = true;
        Scanner scanner = new Scanner(System.in);

        while (wantToPlay) {
            totalCoins = totalCoins - 5;
            int randomInt = ThreadLocalRandom.current().nextInt(1, 47 + 1);
            int guess = 0;
            int numberOfGuesses = 0;
            System.out.println("Er is een willekeurig cijfer geselecteerd");
            System.out.println("Gelieve een cijfer te selecteren");

            while (guess != randomInt) {
                while (scanner.hasNext()) {
                    if (scanner.hasNextInt()) {
                        int val = scanner.nextInt();
                        if (val >= 1 && val <= 47) {
                            guess = val;
                            break;
                        }
                        System.out.println("Gelieve een cijfer tussen 1 en 47 kiezen");
                    } else {
                        System.out.println("Gelieve een cijfer tussen 1 en 47 kiezen");
                        scanner.next();
                    }
                }
                numberOfGuesses = numberOfGuesses + 1;
                if (guess < randomInt) {
                    System.out.println("Hoger");
                } else if (guess > randomInt) {
                    System.out.println("Lager");
                }
            }
            System.out.println(numberOfGuesses);
            switch (numberOfGuesses) {
                case 1:
                    System.out.println("Je hebt 100 coins gewonnen");
                    totalCoins = totalCoins + 100;
                    break;
                case 2:
                    System.out.println("Je hebt 50 coins gewonnen");
                    totalCoins = totalCoins + 50;
                    break;
                case 3:
                    System.out.println("Je hebt 20 coins gewonnen");
                    totalCoins = totalCoins + 20;
                    break;
                case 4:
                    System.out.println("Je hebt 5 coins gewonnen");
                    totalCoins = totalCoins + 5;
                    break;
                default:
                    System.out.println("Je hebt geen coins gewonnen, je hebt nog " + totalCoins + " over");
                    break;
            }


            /**
             * Einde spel, indien mogelijk de keuze om opnieuw te spelen.
             */
            if (totalCoins <5){
                wantToPlay = false;
                break;
            }
            char c = 'a';
            while (c != 'y' && c != 'Y' && c != 'N' && c != 'n') {
                System.out.println("Wilt u nog eens spelen? [Y/N]");
                c = scanner.next().charAt(0);
            }
            if (c == 'n' || c == 'N') {
                wantToPlay = false;
            }
        }

        return totalCoins;
    }

}
