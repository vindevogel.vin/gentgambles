package be.gentgambles.gamblegames;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class SlotMachine {
    public static int slotMachine(int gameCoins) {
        int totalCoins = gameCoins;
        boolean wantToPlay = true;
        Scanner scanner = new Scanner(System.in);

        while (wantToPlay) {
            char keuze = 'Y';
            /**
             * spel starten
             */
            System.out.println("U kan aan de hendel trekken. \nWanneer u aan de hendel trekt verschijnen er 3 willekeurige cijfers tussen 1 en 7. \nWanneer er gelijke cijfers zijn krijgt u een prijs. \nWanneer alle cijfers 3 zijn wint u een jackpot. \n\n \n");
            while (keuze == 'Y' || keuze == 'y') {
                while (keuze != 'N' && keuze != 'n') {
                    if (totalCoins < 5) {
                        return totalCoins;
                    }
                    System.out.println("Wilt u aan de hendel trekken? [Y/N]");
                    keuze = scanner.next().charAt(0);
                    if (keuze == 'y' || keuze == 'Y') {
                        totalCoins = totalCoins - 5;
                        System.out.println("u heeft aan de hendel getrokken");
                        keuze = 'Y';
                        int slot1 = ThreadLocalRandom.current().nextInt(1, 7 + 1);
                        int slot2 = ThreadLocalRandom.current().nextInt(1, 7 + 1);
                        int slot3 = ThreadLocalRandom.current().nextInt(1, 7 + 1);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                        System.out.println(slot1);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                        System.out.println(slot2);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                        System.out.println(slot3);

                        if (slot1 == slot2) {
                            if (slot1 == slot3) {
                                if (slot1 == 3) {
                                    System.out.println("Jackpot!");
                                    totalCoins = totalCoins + 100;
                                    System.out.println("U heeft nog " + totalCoins + " gameCoins over.");

                                } else {
                                    System.out.println("Big win!");
                                    totalCoins = totalCoins + 50;
                                    System.out.println("U heeft nog " + totalCoins + " gameCoins over.");
                                }
                            } else {
                                System.out.println("Small win");
                                totalCoins = totalCoins + 15;
                                System.out.println("U heeft nog " + totalCoins + " gameCoins over.");
                            }
                        } else if (slot1 == slot3) {
                            System.out.println("Small win");
                            totalCoins = totalCoins + 15;
                            System.out.println("U heeft nog " + totalCoins + " gameCoins over.");
                        } else if (slot2 == slot3) {
                            System.out.println("Small win");
                            totalCoins = totalCoins + 15;
                            System.out.println("U heeft nog " + totalCoins + " gameCoins over.");
                        } else {
                            System.out.println("No winners here today!");
                            System.out.println("U heeft nog " + totalCoins + " gameCoins over.");
                        }
                    } else if (keuze == 'n' || keuze == 'N') {
                        wantToPlay = false;
                    }

                }
            }
        }
        return totalCoins;
    }
}
